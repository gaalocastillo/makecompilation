#include<stdio.h>
#include<point.h>

int main(){

  point a;
  point b;

  
  float distancia;
  printf( "Ingrese el valor de la coordenada x del primer punto :\n");
  scanf("%f",&a.x);
  
  printf( "Ingrese el valor de la coordenada y del primer punto :\n");
  scanf("%f",&a.y);
  
  printf( "Ingrese el valor de la coordenada z del primer punto :\n");
  scanf("%f",& a.z);

  printf( "Ingrese el valor de la coordenada x del segundo punto :\n");
  scanf("%f",&b.x);
 
  printf( "Ingrese el valor de la coordenada y del segundo punto :\n");
  scanf("%f",&b.y);
 
  printf( "Ingrese el valor de la coordenada z del segundo punto :\n");
  scanf("%f",&b.z);
  
  distancia = calcularDistanciaEuclidiana(a,b);
  printf("La distancia euclideana entre los puntos a y b es: %.2f", distancia);
  return 0;
}
